<img alt='Pig-game' src='./weather-app/src/assets/Images/weather-logo.svg' width='200px'/>

# Meteorays

## Description

***
 Simple web app where you can see the weather for 7 days from the current day. It shows the weather only for Bulgaria and uses a map where you can choose a district and it will show you the weather for that district.
***

## Installation
***

To start the application you have to clone the repository on your computer, go to the folder, containing the `package.json` file and run `npm install` to install all the dependencies that the project needs to become fully functional (that is, if you already have Node.js installed).
After this, you can run the `npm run dev` to start the app in the browser. Enjoy! 😊

```
npm install
npm run dev
```

## Features and Usage
***
- Displayes the weather for 7 days in each district in Bulgaria
- Allows you to change between dark and light mode

`iPhone 12 Pro` 390 x 844 view

![phone-view](./weather-app/src/assets/Images/Iphone-12-pro.png)

On play `iPad Mini` 768 x 1024

![iPad-view](./weather-app/src/assets/Images/Ipad-mini.png)

Large screen view
`Light mode`

![largeScreen-view](./weather-app/src/assets/Images/Full-view.png)

Large screen view
`Dark mode`

![largeScreen-view](./weather-app/src/assets/Images/Full-view-dark.png)
***

## Technologies used
***

- JavaScript
- ReactJS
- HTML
- CSS
- GitLab
- ESLint
- react-icons
- uuid
- date-fns
