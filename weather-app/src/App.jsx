import './App.css'
import Header from './components/UI/Header/Header'
import WeatherView from './views/WeatherView/WeatherView'

function App() {


  return (
    <div className="App">
        <Header />
        <WeatherView />
    </div>
  )
}

export default App
