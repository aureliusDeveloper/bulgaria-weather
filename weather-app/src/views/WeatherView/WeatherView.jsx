import { format } from 'date-fns';
import React, { useEffect } from 'react';
import { useState } from 'react';
import { v4 } from 'uuid';
import DayCard from '../../components/UI/DayCard/DayCard';
import Map from '../../components/UI/Map/Map';
import { getWeatherDescription } from '../../services/helpers';
import './WeatherView.css';

const WeatherView = () => {
  const [weather, setWeather] = useState(null);
  const [location, setLocation] = useState({
    city: '',
    latitude: '',
    longitude: '',
  });


  useEffect(() => {
    fetch(
      `https://api.open-meteo.com/v1/forecast?latitude=${
        location.latitude || '42.70'
      }&longitude=${
        location.longitude || '23.32'
      }&timezone=auto&daily=apparent_temperature_max,apparent_temperature_min,snowfall_sum,rain_sum,sunrise,sunset,windspeed_10m_max,weathercode`
    )
      .then((res) => res.json())
      .then((data) => setWeather(data));
  }, [location.latitude]);

  return (
    <div className="weather-view__wrapper">
      <Map setLocation={setLocation} />
        <h2>{location.city || 'София-град'}</h2>
      <div className="weather-view-cards__wrapper">
        {weather?.daily ? (
          weather.daily.time.map((time, idx) => (
            <DayCard
              key={v4()}
              date={format(new Date(time), 'yyyy/MMM/do')}
              day={format(new Date(time), 'EEEE')}
              sunrise={format(new Date(weather?.daily.sunrise[idx]), 'HH:mm a')}
              sunset={format(new Date(weather?.daily.sunset[idx]), 'HH:mm a')}
              weatherCode={getWeatherDescription(
                weather?.daily.weathercode[idx]
              )}
              tempMax={weather?.daily.apparent_temperature_max[idx]}
              tempMin={weather?.daily.apparent_temperature_min[idx]}
              rainSum={weather?.daily.rain_sum[idx]}
              snowfallSum={weather?.daily.snowfall_sum[idx]}
              windspeedMax={weather?.daily.windspeed_10m_max[idx]}
              dayNumber={idx + 1}
            />
          ))
        ) : (
          <></>
        )}
      </div>
    </div>
  );
};

export default WeatherView;


