import React from 'react';
import './DayCard.css';
import { ImArrowUp } from 'react-icons/im';
import { ImArrowDown } from 'react-icons/im';
import { WiDaySunny } from 'react-icons/wi';
import { days } from '../../../common/enums';

const DayCard = ({
  date,
  day,
  sunrise,
  sunset,
  weatherCode,
  tempMax,
  tempMin,
  rainSum,
  snowfallSum,
  windspeedMax
}) => {
  return (
    <div className="day-card__wrapper">
      <div className="day-card__header">
        <div className="header-day__status">
          <div className="header-day__date">
            <time>{date}</time>
            <time>{days[day]}</time>
          </div>
          <span className="header-day__code">{weatherCode}</span>
        </div>
        <div className="header-sunset">
          <div className="header-sunset__icons">
            <ImArrowUp />
            <ImArrowDown />
          </div>
          <div className="header-sunset__time">
            <WiDaySunny />
            <p>{sunrise}</p>
            <p>{sunset}</p>
          </div>
        </div>
      </div>
      <div className="day-card__main">
        <p>{`Мин. температура: ${tempMin} ℃`}</p>
        <p>{`Макс. температура: ${tempMax} ℃`}</p>
        <p>{`Кол-во. валежи: ${rainSum} мм.`}</p>
        <p>{`Кол-во. снеговалежи: ${snowfallSum} см.`}</p>
        <p>{`Макс. скорост на вятъра: ${windspeedMax} км/ч.`}</p>
      </div>
    </div>
  );
};

export default DayCard;
