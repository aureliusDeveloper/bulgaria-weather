import React, { useEffect, useState } from 'react';
import './Header.css';
import logo from '../../../assets/Images/weather-logo.svg';
import { themeEnum } from '../../../common/enums';
import lightThemeIcon from '../../../assets/Images/light.svg';
import darkThemeIcon from '../../../assets/Images/dark.svg';

const Header = () => {
  const [theme, setTheme] = useState(localStorage.getItem('theme') || themeEnum.light);

  useEffect(() => {
    const element = document.body;
      element.className = theme;
  }, [theme]);

    const toggleThemeHandler = () => {
      if (theme === themeEnum.light) {
        localStorage.setItem('theme', themeEnum.dark);
        setTheme(themeEnum.dark);
      } else {
        localStorage.setItem('theme', themeEnum.light);
        setTheme(themeEnum.light);
      }
    }

  return (
    <div className="header__wrapper">
      <div className="header-logo__wrapper">
        <img src={logo} alt="weather-logo" />
        <p className="header-logo__desc">
            Meteorays
        </p>
      </div>
      <section className="header-title__wrapper">
        <h1>Добре дошли в Meteorays!</h1>
        <p>
         Разберете какво е времето за 7 дни като изберете област и планирайте вашите пътувания по-добре!
        </p>
      </section>
      <div className='header-theme'>
        <img src={theme === themeEnum.light ? darkThemeIcon : lightThemeIcon} alt="theme-icons-moon-or-sun" role="button" onClick={toggleThemeHandler}/>
      </div>
    </div>
  );
};

export default Header;
