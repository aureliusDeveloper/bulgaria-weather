export const days = Object.freeze({
Monday: 'Понеделник',
Tuesday: 'Вторник',
Wednesday: 'Сряда',
Thursday: 'Четвъртък',
Friday: 'Петък',
Saturday: 'Събота',
Sunday: 'Неделя',

});


export const themeEnum = Object.freeze({
    light: 'light',
    dark: 'dark'
});
