export const getWeatherDescription = (code) => {
  switch (code) {
    case 0: {
      return 'Слънчево/ясно';
    }
    case 1: {
      return 'Предимно чисто небе';
    }
    case 2: {
      return 'Разкъсана облачност';
    }
    case 3: {
      return 'Значителна облачност';
    }
    case 45: {
      return 'Мъгливо';
    }
    case 48: {
      return 'Мъгла/слана';
    }
    case 56: {
      return 'Слаб ръмеж';
    }
    case 57: {
     return 'Умерен ръмеж';
    }
    case 61: {
        return 'Слаб дъжд';
    }
    case 63: {
        return 'Умерен дъжд';
    }
    case 65: {
        return 'Силен дъжд';
    }
    case 66: {
        return 'Слаба суграшица';
    }
    case 67: {
        return 'Силна суграшица';
    }
    case 71: {
        return 'Слаб снеговалеж';
    }
    case 73: {
        return 'Умерен снеговалеж';
    }
    case 75: {
        return 'Силен снеговалеж';
    }
    case 77: {
        return 'Снежни зърна'
    }
    case 80: {
       return 'Силен дъжд'
    }
    case 81: {
        return 'Умерен дъжд'
    }
    case 82: {
        return 'Буря'
    }
    case 85: {
        return 'Слаба снежна буря'
    }
    case 86: {
        return 'Силна снежна буря'
    }
    case 95: {
        return 'Гръмотевична буря'
    }
    case 96: {
        return 'Гръмотевична буря придружена с градушка'
    }
    case 99: {
        return 'Гръмотевична буря придружена с градушка'
    }
  }
};
